def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, time = 2)
  arr = []
  time.times do
    arr << str
  end
  arr.join(" ")
end

def start_of_word(str, index = 1)
  str[0..index - 1]
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  smalls = ["and", "the", "over"]
  words = str.split.map.with_index do |word, i|
    if i != 0 && smalls.include?(word)
      word
    else
      word.capitalize
    end
  end
  words.join(" ")
end
