def ftoc(temp)
  5.0/9 * (temp - 32)
end

def ctof(temp)
  (9.0/5 * temp) + 32
end
