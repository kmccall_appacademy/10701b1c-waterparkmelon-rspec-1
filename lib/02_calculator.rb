def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  if arr.empty?
    0
  else
    arr.reduce(:+)
  end
end

def multiply
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  product = 1
  if num == 1
    1
  else
    product * factorial(num - 1)
  end
  product
end
