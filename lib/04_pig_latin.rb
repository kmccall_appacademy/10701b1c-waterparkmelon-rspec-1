def translate(word)
  word.split.map {|word| latinize(word)}.join(" ")
end

def latinize(word)
  if "aeiou".include?(word[0].downcase)
    word + "ay"
  else
    chunk = 0
    word.chars.each_index do |i|
      if "aeiou".include?(word[i].downcase)
        break
      elsif word[i] == "q" && word[i+1] == "u"
        chunk += 2
        break
      else
        chunk += 1
      end
    end
    word.chars.drop(chunk).join + word.chars.take(chunk).join + "ay"
  end
end
